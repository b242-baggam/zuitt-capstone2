import express from "express";
// const express = require("express");

import User from "../models/User.js"
// const User = require("../models/User.js");

import Product from "../models/Product.js";
// const Product = require("../models/Product.js");

var router = express.Router();



router.get("/userDetails", async (req, res) => {
    if (req.user.isAdmin) { 
         await User.findOne({ email : req.user.email}) // name : ,description : ,price : 
         return res.send(data)
    }
    return res.send("Dont have permission")
})

router.get("/createOrder", async (req,res) => { 
    let products = req.body ;
    let total = 0 
    console.log(products);
    products.map(({ productName, quantity })=> {
        console.log(productName);
        console.log(quantity);
        Product.findOne({ name : productName }).then((res) => { 
            total += res.price * quantity 
        })
    }) 
    await User.updateOne({ email : req.user.email } , { "$push" : { 
        orders : { products : products , totalAmount : total }
    }})
    return res.send(true)
})


export default router;
// module.exports = router;